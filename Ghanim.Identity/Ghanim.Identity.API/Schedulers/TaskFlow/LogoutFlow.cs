﻿using AutoMapper;
using Ghanim.Identity.BLL.Managers;
using Ghanim.Identity.Models.Context;
using Ghanim.Order.API;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Identity.API.Schedulers.TaskFlow
{
    public class LogoutFlow
    {
        ApplicationDbContext context;
        private readonly ILogger<LogoutFlow> logger;
        private readonly IMapper mapper;

        //ArchivedOrderManager archivedOrderManager;
        ApplicationUserHistoryManager applicationUserHistoryManager;


        public LogoutFlow(IServiceProvider _serviceProvider, IMapper _mapper)
        {
            context = StaticAppSettings.GetDbContext();
            //  archivedOrderManager = new ArchivedOrderManager(context);
            applicationUserHistoryManager = new ApplicationUserHistoryManager(context);
            mapper = _mapper;

            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                logger = _serviceProvider.GetService<ILogger<LogoutFlow>>();
            }
        }

        public async void LogoutService()
        {
            var LogoutRes = await applicationUserHistoryManager.GetLoginUsers();

            DateTime currentTime = DateTime.Now;

            if (LogoutRes.IsSucceeded && LogoutRes.Data != null && LogoutRes.Data.Count > 0)
            {
                foreach (var item in LogoutRes.Data)
                {
                    item.LogoutDate = DateTime.Now;
                    var logoutUserRes = applicationUserHistoryManager.Update(item);
                }

            }
        }
    }
}
