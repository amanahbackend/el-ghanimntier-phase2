﻿using Ghanim.Identity.BLL.IManagers;
using Ghanim.Identity.Models.Context;
using Ghanim.Identity.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Identity.API.Seed
{
    public class ApplicationDbContextSeed
    {
        IApplicationUserManager applicationUserManager;
        IApplicationRoleManager applicationRoleManager;

        public ApplicationDbContextSeed(IApplicationUserManager _applicationUserManager,
            IApplicationRoleManager _applicationRoleManager)
        {
            applicationUserManager = _applicationUserManager;
            applicationRoleManager = _applicationRoleManager;
        }
        public async Task SeedAsync(ApplicationDbContext context, IHostingEnvironment env,
            ILogger<ApplicationDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
                if (!context.Roles.Any())
                {
                    await applicationRoleManager.AddRolesAsync(GetDefaultRoles());
                }
                if (!context.Users.Any())
                {
                    await applicationUserManager.AddUsersAsync(GetDefaultUsers());
                }
                //if (!context.Countries.Any())
                //{
                //    countryManager.Seed();
                //}
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }

        private List<Tuple<ApplicationUser, string>> GetDefaultUsers()
        {
            var user1 = new ApplicationUser()
            {
                FirstName = "admin",
                LastName = "admin",
                Phone1 = "12345678",
                UserName = "admin",
                Email = "admin@microsoft.com",
                RoleNames = new List<string> { "Admin" },
                Deactivated = false,
                PhoneNumberConfirmed = true,
                EmailConfirmed = true
            };
            var user2 = new ApplicationUser()
            {
                FirstName = "Supervisor",
                LastName = "Supervisor",
                Phone1 = "87654321",
                UserName = "Supervisor",
                Email = "Supervisor@microsoft.com",
                RoleNames = new List<string> { "Supervisor" },
                Deactivated = false,
                PhoneNumberConfirmed = true,
                EmailConfirmed = true
            };
            var user3 = new ApplicationUser()
            {
                FirstName = "Dispatcher",
                LastName = "Dispatcher",
                Phone1 = "56487321",
                UserName = "Dispatcher",
                Email = "Dispatcher@microsoft.com",
                RoleNames = new List<string> { "Dispatcher" },
                Deactivated = false,
                PhoneNumberConfirmed = true,
                EmailConfirmed = true
            };
            return new List<Tuple<ApplicationUser, string>>()
            {
                new Tuple<ApplicationUser, string>(user1,"P@ssw0rd"),
                new Tuple<ApplicationUser, string>(user2,"P@ssw0rd"),
                new Tuple<ApplicationUser, string>(user3,"P@ssw0rd"),
            };
        }

        private List<ApplicationRole> GetDefaultRoles()
        {
            return new List<ApplicationRole>()
            {
                new ApplicationRole(){Name="Admin"},
                new ApplicationRole(){Name="Supervisor"},
                new ApplicationRole(){Name="Dispatcher"},
                new ApplicationRole(){Name="Engineer"},
                new ApplicationRole(){Name="Foreman"},
                new ApplicationRole(){Name="Technician"},
                new ApplicationRole(){Name="Driver"}
            };
        }
    }
}
