﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.Identity.Models.Entities;

namespace Ghanim.Identity.Models.EntitiesConfiguration
{
    public class ApplicationRoleTypeConfiguration
        : IdentityBaseEntityTypeConfiguration<ApplicationRole>, IEntityTypeConfiguration<ApplicationRole>
    {
        public new void Configure(EntityTypeBuilder<ApplicationRole> builder)
        {
            base.Configure(builder);
            //builder.Property(r => r.CreatedDate).IsRequired();
            //builder.Property(r => r.DeletedDate).IsRequired();
            //builder.Property(r => r.UpdatedDate).IsRequired();
            //builder.Property(r => r.FK_UpdatedBy_Id).IsRequired(false);
            //builder.Property(r => r.FK_DeletedBy_Id).IsRequired(false);
            //builder.Property(r => r.FK_CreatedBy_Id).IsRequired(false);
            //builder.Property(r => r.IsDeleted).IsRequired(true);
        }
    }
}
