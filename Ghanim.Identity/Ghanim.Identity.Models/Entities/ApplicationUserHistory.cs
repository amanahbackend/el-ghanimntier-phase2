﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.Identity.Models.Entities
{
   public class ApplicationUserHistory  : BaseEntity
    {
        public string UserId { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public string Token { get; set; }
        public string UserType { get; set; }
        public string DeveiceId { get; set; }
    }
}
