﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.Order.EFCore.MSSQL.Migrations
{
    public partial class addIsReadtonotificationCenterTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsRead",
                table: "NotificationCenter",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRead",
                table: "NotificationCenter");
        }
    }
}
