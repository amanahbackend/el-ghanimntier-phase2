﻿using DispatchProduct.RepositoryModule;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Order.API.Seed
{
    public class OrderDbContextSeed : ContextSeed
    {
        public async Task SeedAsync(OrderDBContext context, IHostingEnvironment env,
            ILogger<OrderDbContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                // settings = appSettings.Value;
                var contentRootPath = env.ContentRootPath;
                var webroot = env.WebRootPath;

                var settings = GetDefaultSettings();

                using (var transaction = context.Database.BeginTransaction())
                {
                    await SeedEntityAsync(context, settings, false);

                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Settings ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Settings OFF");

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 3)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for OrderDbContext");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }

        private List<OrderSetting> GetDefaultSettings()
        {
            return new List<OrderSetting>()
            {
                new OrderSetting()
                {
                    Id = 1,
                    Name = "ExceedTimeHours",
                    Value = "24"
                },
                new OrderSetting()
                {
                    Id = 2,
                    Name = "ArchiveAfterDays",
                    Value = "15"
                }
            };
        }
    }
}
