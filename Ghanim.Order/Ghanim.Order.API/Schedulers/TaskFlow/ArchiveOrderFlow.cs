﻿using AutoMapper;
using Ghanim.Order.BLL.Managers;
using Ghanim.Order.EFCore.MSSQL.Context;
using Ghanim.Order.Models.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ghanim.Order.API.Schedulers.TaskFlow
{
    public class ArchiveOrderFlow
    {
        OrderDBContext context;
        private readonly ILogger<ArchiveOrderFlow> logger;

        private readonly new IMapper mapper;

        ArchivedOrderManager archivedOrderManager;
        OrderManager orderManager;
        OrderSettingManager orderSettingManager;

        public ArchiveOrderFlow(IServiceProvider _serviceProvider, IMapper _mapper)
        {
            context = StaticAppSettings.GetDbContext();
            archivedOrderManager = new ArchivedOrderManager(context);
            orderManager = new OrderManager(context);
            orderSettingManager = new OrderSettingManager(context);
            mapper = _mapper;
        }

        public async void Archive()
        {
            var archiveAfterDays = orderSettingManager.GetAllQuerable().Data.Where(x => x.Name == StaticAppSettings.ArchiveAfterDays).FirstOrDefault().Value;
            int daysAfter = Convert.ToInt32(archiveAfterDays);

            DateTime currentTime = DateTime.Now;

            var orders = orderManager.GetAllQuerable().Data.Where(x => StaticAppSettings.ArchivedStatusId.Contains(x.StatusId) && (currentTime - x.CreatedDate).TotalDays >= daysAfter).ToList();

            if (orders.Count > 0)
            {
                var archivedOrders = mapper.Map<List<OrderObject>, List<ArchivedOrder>>(orders);
                var archiveResult = archivedOrderManager.Add(archivedOrders);
                if (archiveResult.IsSucceeded)
                {
                    orderManager.Delete(orders);
                }
            }
        }

    }
}
