﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.Order.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TeamMembersService
{
    public class TeamMembersService : DefaultHttpClientCrud<UserManagementServiceSettings, TeamMemberViewModel, TeamMemberViewModel>,
       ITeamMembersService
    {
        UserManagementServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public TeamMembersService(IOptions<UserManagementServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _settings = obj.Value;
            _dnsQuery = dnsQuery;
        }

        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMembersByTeamId(int teamId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetMembersByTeamId;
                var url = $"{baseUrl}/{requestedAction}/{teamId}";
                return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<TeamMemberViewModel>>> GetMemberUsersByTeamId(int teamId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetMemberUsersByTeamId;
                var url = $"{baseUrl}/{requestedAction}/{teamId}";
                return await GetByUriCustomized<List<TeamMemberViewModel>>(url);
            }
            return null;
        }

    

    }
}