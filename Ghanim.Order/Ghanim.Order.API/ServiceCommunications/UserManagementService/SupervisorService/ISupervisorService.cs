﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.SupervisorService
{
    public interface ISupervisorService : IDefaultHttpClientCrud<UserManagementServiceSettings, SupervisorViewModel, SupervisorViewModel>
    {
        Task<ProcessResultViewModel<List<SupervisorViewModel>>> GetSupervisorsByDivisionId(int divisonId);
        Task<ProcessResultViewModel<SupervisorViewModel>> GetSupervisorByDivisionId(int divisonId);
        Task<ProcessResultViewModel<SupervisorViewModel>> GetSupervisorByUserId(string userId);
    }
}