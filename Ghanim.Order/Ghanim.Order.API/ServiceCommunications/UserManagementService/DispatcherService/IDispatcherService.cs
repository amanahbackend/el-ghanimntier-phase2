﻿using DispatchProduct.HttpClient;
using Ghanim.Order.API.ServiceCommunications.UserManagementService;
using Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.DispatcherService
{
    public interface IDispatcherService : IDefaultHttpClientCrud<UserManagementServiceSettings, DispatcherViewModel, DispatcherViewModel>
    {
        Task<ProcessResultViewModel<List<DispatcherViewModel>>> GetDispatchersByDivision(int divisonId);
        Task<ProcessResultViewModel<DispatcherViewModel>> GetDispatcherById(int id);
        Task<ProcessResultViewModel<DispatcherViewModel>> GetDispatcherByUserId(string userId);
        
    }
}