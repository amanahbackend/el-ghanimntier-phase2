﻿using DispatchProduct.HttpClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Order.API.ServiceCommunications.UserManagementService.TechnicianStateService
{
    public interface ITechnicianStateService : IDefaultHttpClientCrud<UserManagementServiceSettings, TechnicianStateViewModel, TechnicianStateViewModel>
    {
        Task<ProcessResultViewModel<TechnicianStateViewModel>> Add(TechnicianStateViewModel model);
    }
}