﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class ResetPasswordViewModel
    {
        [Required]
        public List<string> Usernames { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}
