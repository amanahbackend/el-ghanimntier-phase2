﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.API.ViewModels
{
    public class TeamResetPasswordViewModel
    {
        [Required]
        public int TeamId { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}
