﻿using DispatchProduct.HttpClient;
using DnsClient;
using Ghanim.UserManagement.API.ViewModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.UserManagement.API.ServiceCommunications.Identity
{
    public class IdentityUserService
       : DefaultHttpClientCrud<IdentityServiceSettings, ApplicationUserViewModel, ApplicationUserViewModel>,
       IIdentityUserService
    {
        IdentityServiceSettings _settings;
        IDnsQuery _dnsQuery;

        public IdentityUserService(IOptions<IdentityServiceSettings> obj, IDnsQuery dnsQuery) : base(obj.Value, dnsQuery)
        {
            _dnsQuery = dnsQuery;
            _settings = obj.Value;
        }

        public async Task<ProcessResultViewModel<ApplicationUserViewModel>> GetById(string userId)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetUserById;
                var url = $"{baseUrl}/{requestedAction}?id={userId}";
                return await Get(url);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<List<ApplicationUserViewModel>>> GetByUserIds(List<string> ids)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.GetByIdsAction;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<List<string>, List<ApplicationUserViewModel>>(url, ids);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<bool>> ResetPassword(ResetPasswordViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.ResetPassword;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<ResetPasswordViewModel,bool>(url, model);
            }
            return null;
        }

        public async Task<ProcessResultViewModel<LoginResultViewModel>> Login(LoginViewModel model)
        {
            string baseUrl = await _settings.GetBaseUrl(_dnsQuery);
            while (baseUrl.EndsWith('/'))
            {
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            }
            if (!String.IsNullOrEmpty(baseUrl))
            {
                string requestedAction = _settings.Login;
                var url = $"{baseUrl}/{requestedAction}";
                return await PostCustomize<LoginViewModel, LoginResultViewModel>(url, model);
            }
            return null;
        }
    }
}
