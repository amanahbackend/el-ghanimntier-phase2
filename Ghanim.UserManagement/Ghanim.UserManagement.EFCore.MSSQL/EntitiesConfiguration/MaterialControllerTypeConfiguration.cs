﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.EFCore.MSSQL.EntitiesConfiguration
{
    class MaterialControllerTypeConfiguration : BaseEntityTypeConfiguration<MaterialController>
    {
        public void Configure(EntityTypeBuilder<MaterialController> builder)
        {
            base.Configure(builder);
            builder.ToTable("MaterialController");
        }
    }
}
