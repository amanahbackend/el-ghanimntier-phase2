﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class TeamMemberTypeConfiguration : BaseEntityTypeConfiguration<TeamMember>
    {
        public void Configure(EntityTypeBuilder<TeamMember> builder)
        {
            base.Configure(builder);
            builder.ToTable("TeamMember");
            builder.Property(o => o.TeamId).IsRequired(false);
        }
    }
}