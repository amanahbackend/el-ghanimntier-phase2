﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.UserManagement.EFCore.MSSQL.Migrations
{
    public partial class addmembertypenameandpftoteammember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MemberTypeName",
                table: "Members",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PF",
                table: "Members",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MemberTypeName",
                table: "Members");

            migrationBuilder.DropColumn(
                name: "PF",
                table: "Members");
        }
    }
}
