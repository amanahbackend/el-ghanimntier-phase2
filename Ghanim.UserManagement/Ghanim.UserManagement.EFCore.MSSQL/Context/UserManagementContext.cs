﻿using Ghanim.UserManagement.EFCore.MSSQL.EntitiesConfiguration;
using Ghanim.UserManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.EFCore.MSSQL.Context
{
    public class UserManagementContext : DbContext
    {
        public UserManagementContext(DbContextOptions<UserManagementContext> options) : base(options)
        {
        }

        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Dispatcher> Dispatchers { get; set; }
        public DbSet<Engineer> Engineers { get; set; }
        public DbSet<Foreman> Foremans { get; set; }
        public DbSet<Technician> Technicians { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<TeamMember> Members{ get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Attendence> Attendences { get; set; }
        public DbSet<DispatcherSettings> DispatcherSettings { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<MaterialController> MaterialControllers { get; set; }
        public DbSet<TechniciansState> TechniciansStates { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new SupervisorTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DispatcherSettingsTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DispatcherTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EngineerTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ForemanTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TechnicianTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DriverTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TeamMemberTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TeamTypeConfiguration());
        }
    }
}
