﻿using DispatchProduct.RepositoryModule;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.UserManagement.EFCore.MSSQL.Context
{
    public class DataContextSeed : ContextSeed
    {

        public DataContextSeed()
        {
        }


        public async Task SeedAsync(UserManagementContext context, IHostingEnvironment env,
            ILogger<DataContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
               
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, retryForAvaiability);
                }
            }
        }
    }
}
