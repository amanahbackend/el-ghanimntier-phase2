﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.Models.IEntities
{
  public  interface IDriver :IBaseEntity
    {
        string UserId { get; set; }
        string Name { get; set; }
        int DivisionId { get; set; }
        string DivisionName { get; set; }
        int CostCenterId { get; set; }
        string CostCenterName { get; set; }
        string VehicleNo { get; set; }
        string PF { get; set; }
    }
}
