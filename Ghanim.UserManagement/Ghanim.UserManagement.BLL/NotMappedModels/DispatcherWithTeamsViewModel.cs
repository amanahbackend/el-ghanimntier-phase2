﻿using Ghanim.UserManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.UserManagement.BLL.NotMappedModels
{
    public class DispatcherWithTeamsViewModel
    {
        public int DispatcherId { get; set; }
        public string DispatcherName { get; set; }
        public IEnumerable<Team> Teams { get; set; }
    }
}