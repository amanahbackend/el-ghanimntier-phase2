﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class Lang_RejectionReasonManager : Repository<Lang_RejectionReason>, ILang_RejectionReasonManager
    {
        public Lang_RejectionReasonManager(LookUpDbContext context)
            : base(context)
        {
        }
    }
}