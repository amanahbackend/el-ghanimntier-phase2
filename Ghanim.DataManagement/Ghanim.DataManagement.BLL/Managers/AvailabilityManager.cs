﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class AvailabilityManager : Repository<Availability>, IAvailabilityManager
    {
        public AvailabilityManager(LookUpDbContext context)
            : base(context)
        {
        }
    }
}
