﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class OrderStatusManager : Repository<OrderStatus>, IOrderStatusManager
    {
        public OrderStatusManager(LookUpDbContext context)
            : base(context)
        {
        }

        public ProcessResult<List<OrderStatus>> GetAllExclude(List<int> excludedStatuseIds)
        {
            return GetAll(x => !excludedStatuseIds.Contains(x.Id));
        }
    }
}