﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class OrderSubStatusManager : Repository<OrderSubStatus>, IOrderSubStatusManager
    {
        public OrderSubStatusManager(LookUpDbContext context)
            : base(context)
        {
        }

        public ProcessResult<List<OrderSubStatus>> GetByStatusId(int statusId)
        {
            return GetAll(x => x.StatusId == statusId);
        }
    }
}