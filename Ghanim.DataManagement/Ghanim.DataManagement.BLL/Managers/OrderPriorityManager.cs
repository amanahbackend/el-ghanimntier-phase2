﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.EFCore.MSSQL.Context;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.BLL.Managers
{
    public class OrderPriorityManager : Repository<OrderPriority>, IOrderPriorityManager
    {
        public OrderPriorityManager(LookUpDbContext context)
            : base(context)
        {
        }
    }
}
