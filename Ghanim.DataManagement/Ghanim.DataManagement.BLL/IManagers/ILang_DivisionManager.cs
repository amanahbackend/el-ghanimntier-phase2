﻿using DispatchProduct.RepositoryModule;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.BLL.IManagers
{
    public interface ILang_DivisionManager : IRepository<Lang_Division>
    {
        ProcessResult<List<Lang_Division>> GetAllLanguagesByDivisionId(int DivisionId);
        ProcessResult<bool> UpdateByDivision(List<Lang_Division> entities);
    }
}
