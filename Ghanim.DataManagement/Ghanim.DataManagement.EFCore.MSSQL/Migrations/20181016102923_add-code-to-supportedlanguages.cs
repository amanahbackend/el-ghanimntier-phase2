﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.DataManagement.EFCore.MSSQL.Migrations
{
    public partial class addcodetosupportedlanguages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "SupportedLanguages",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "SupportedLanguages");
        }
    }
}
