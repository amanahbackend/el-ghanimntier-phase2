﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ghanim.DataManagement.EFCore.MSSQL.Migrations
{
    public partial class addordertables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Lang_OrderPriority",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_OrderPriority_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_OrderPriority", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_OrderProblem",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_OrderProblem_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_OrderProblem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_OrderStatus",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_OrderStatus_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_OrderStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_OrderSubStatus",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_OrderSubStatus_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_OrderSubStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lang_OrderType",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FK_OrderType_ID = table.Column<int>(nullable: false),
                    FK_SupportedLanguages_ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lang_OrderType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderPriority",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderPriority", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderProblem",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderProblem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderStatus",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderSubStatus",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    StatusId = table.Column<int>(nullable: false),
                    StatusName = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderSubStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderType",
                columns: table => new
                {
                    IsDeleted = table.Column<bool>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderType", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Lang_OrderPriority");

            migrationBuilder.DropTable(
                name: "Lang_OrderProblem");

            migrationBuilder.DropTable(
                name: "Lang_OrderStatus");

            migrationBuilder.DropTable(
                name: "Lang_OrderSubStatus");

            migrationBuilder.DropTable(
                name: "Lang_OrderType");

            migrationBuilder.DropTable(
                name: "OrderPriority");

            migrationBuilder.DropTable(
                name: "OrderProblem");

            migrationBuilder.DropTable(
                name: "OrderStatus");

            migrationBuilder.DropTable(
                name: "OrderSubStatus");

            migrationBuilder.DropTable(
                name: "OrderType");
        }
    }
}
