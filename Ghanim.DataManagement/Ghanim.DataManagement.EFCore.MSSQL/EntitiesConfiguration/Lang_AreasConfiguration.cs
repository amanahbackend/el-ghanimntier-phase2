﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
    public class Lang_AreasConfiguration : BaseEntityTypeConfiguration<Lang_Areas>, IEntityTypeConfiguration<Lang_Areas>
    {
        public override void Configure(EntityTypeBuilder<Lang_Areas> Lang_AreasConfiguration)
        {
            base.Configure(Lang_AreasConfiguration);

            Lang_AreasConfiguration.ToTable("Lang_Areas");
            Lang_AreasConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            Lang_AreasConfiguration.Property(o => o.Name).IsRequired();
            Lang_AreasConfiguration.Property(o => o.FK_Area_ID).IsRequired();
            Lang_AreasConfiguration.Property(o => o.FK_SupportedLanguages_ID).IsRequired();

        }
    }
}
