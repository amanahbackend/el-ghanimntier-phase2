﻿using DispatchProduct.BuildingBlocks.BaseEntities.EntityConfigurations;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.EFCore.MSSQL.EntitiesConfiguration
{
   public class ContractTypesConfiguration : BaseEntityTypeConfiguration<ContractTypes>, IEntityTypeConfiguration<ContractTypes>
   {
        public override void Configure(EntityTypeBuilder<ContractTypes> ContractTypesConfiguration)
        {
        base.Configure(ContractTypesConfiguration);

            ContractTypesConfiguration.ToTable("ContractTypes");
            ContractTypesConfiguration.Property(o => o.Id).ValueGeneratedOnAdd();
            ContractTypesConfiguration.Property(o => o.Code).IsRequired();
            ContractTypesConfiguration.Property(o => o.Name).IsRequired();
        }
   }
}
