﻿using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Microsoft.Extensions.DependencyInjection;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class CostCenterController : BaseController<ICostCenterManager, CostCenter, CostCenterViewModel>
    {
        IServiceProvider _serviceprovider;
        ICostCenterManager manager;
        IProcessResultMapper processResultMapper;
        public CostCenterController(IServiceProvider serviceprovider, ICostCenterManager _manager, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manager, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            _serviceprovider = serviceprovider;
            manager = _manager;
            processResultMapper = _processResultMapper;
        }
        [HttpPost]
        [Route("Add")]
        public override ProcessResultViewModel<CostCenterViewModel> Post([FromBody] CostCenterViewModel model)
        {
            ProcessResultViewModel<CostCenterViewModel> result = null;
            var costCenterIsExit = manager.costCenterIsExit(model.Name);
            if (costCenterIsExit!=null&& costCenterIsExit.Data)
            {
                result = ProcessResultViewModelHelper.Failed<CostCenterViewModel>(null,"there is Cost Center with the same name.");
            }
            else {
                result = base.Post(model);
            }          
            return result;
        }

        [HttpPost]
        [Route("AddMulti")]
        public override ProcessResultViewModel<List<CostCenterViewModel>> PostMulti([FromBody] List<CostCenterViewModel> lstModel)
        {
            ProcessResultViewModel<List<CostCenterViewModel>> result = null;
            List<CostCenterViewModel> addedCostCenter = new List<CostCenterViewModel>();
            foreach (var item in lstModel)
            {
                var costCenterIsExit = manager.costCenterIsExit(item.Name);
                if (costCenterIsExit != null && costCenterIsExit.Data)
                {
                    result = ProcessResultViewModelHelper.Failed<List<CostCenterViewModel>>(null, "there is Cost Center with this name:" + item.Name);
                }
                else
                {
                    addedCostCenter.Add(item);
                }

            }
            result = base.PostMulti(addedCostCenter);

            return result;
        }
        //private ILang_CostCenterManager Lang_Areasmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ILang_CostCenterManager>();
        //    }
        //}
        //private ISupportedLanguagesManager SupportedLanguagesmanager
        //{
        //    get
        //    {
        //        return _serviceprovider.GetService<ISupportedLanguagesManager>();
        //    }
        //}
        //[HttpGet]
        //[Route("GetAreaByLanguage")]
        //public ProcessResultViewModel<AreasViewModel> GetAreaByLanguage([FromQuery]int AreaId, string Language_code)
        //{
        //    var SupportedLanguagesRes = SupportedLanguagesmanager.Get(x => x.Code == Language_code);
        //    var Lang_AreasRes = Lang_Areasmanager.Get(x => x.FK_SupportedLanguages_ID == SupportedLanguagesRes.Data.Id);
        //    var entityResult = manager.Get(AreaId);
        //    entityResult.Data.Name = Lang_AreasRes.Data.Name;
        //    var result = processResultMapper.Map<Areas, AreasViewModel>(entityResult);
        //    return result;
        //}


        //[HttpGet]
        //[Route("GetAreaWithAllLanguages/{AreaId}")]
        //public ProcessResultViewModel<List<AreasViewModel>> GetAreaWithAllLanguages([FromRoute]int AreaId)
        //{
        //    ProcessResult<List<Areas>> entityResult = null;
        //    var Lang_AreasRes = Lang_Areasmanager.GetAll().Data.FindAll(x => x.FK_Area_ID == AreaId);
        //    foreach (var item in Lang_AreasRes)
        //    {
        //        var AreasRes = manager.Get(x => x.Id == item.FK_Area_ID);
        //        AreasRes.Data.Name = item.Name;
        //        entityResult.Data.Add(AreasRes.Data);
        //    }
        //    var result = processResultMapper.Map<List<Areas>, List<AreasViewModel>>(entityResult);
        //    return result;
        //}
    }
}
