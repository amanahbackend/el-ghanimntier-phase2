﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DispatchProduct.Controllers;
using Ghanim.DataManagement.API.ViewModels;
using Ghanim.DataManagement.BLL.IManagers;
using Ghanim.DataManagement.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderStatusController : BaseController<IOrderStatusManager, OrderStatus, OrderStatusViewModel>
    {
        IOrderStatusManager orderStatusManager;
        IMapper mapper;
        public OrderStatusController(IOrderStatusManager _manger, IMapper _mapper, IProcessResultMapper _processResultMapper, IProcessResultPaginatedMapper _processResultPaginatedMapper) : base(_manger, _mapper, _processResultMapper, _processResultPaginatedMapper)
        {
            orderStatusManager = _manger;
            mapper = _mapper;
        }

        [HttpGet]
        [Route("GetAllExcluded")]
        public ProcessResult<List<OrderStatusViewModel>> GetAllExcluded()
        {
            var actions = orderStatusManager.GetAllExclude(StaticAppSettings.ExcludedStatusIds);

            if (actions.IsSucceeded && actions.Data.Count > 0)
            {
                var result = mapper.Map<List<OrderStatus>, List<OrderStatusViewModel>>(actions.Data);
                return ProcessResultHelper.Succedded<List<OrderStatusViewModel>>(result);
            }
            return ProcessResultHelper.Failed<List<OrderStatusViewModel>>(null, null, "something wrong while getting data from get all excluded");
        }
    }
}