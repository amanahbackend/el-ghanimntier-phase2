﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Ghanim.DataManagement.BLL.ExcelSettings;
using Ghanim.DataManagement.BLL.IManagers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utilites.UploadFile;

namespace Ghanim.DataManagement.API.Controllers
{
    [Route("api/[controller]")]
    public class OrderDataController : ControllerBase
    {
        public IReadOrderManager manger;
        public readonly IMapper mapper;
        public OrderDataController(IReadOrderManager _manger, IMapper _mapper)
        {
            this.manger = _manger;
            this.mapper = _mapper;
        }
        // GET: api/values

        [HttpPost]
        [Route("UploadOrders")]
        public IActionResult UploadOrders([FromBody]UploadFile file)
        {
            //if (settings.ExclPath == null)
            //{
            //    settings.ExclPath = "SalarySheet";
            //}
            var result = manger.Process("OrderData", file);
            if (result.IsSucceeded)
            {
                return Ok(result.Data);
            }
            else
            {
                return Ok(result.Exception);
            }
        }
    }
}