﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.DataManagement.API
{
    public static class StaticAppSettings
    {
        public static List<int> ExcludedStatusIds { get; set; }
        public static List<string> ExcludedStatusNames { get; set; }
    }
}
