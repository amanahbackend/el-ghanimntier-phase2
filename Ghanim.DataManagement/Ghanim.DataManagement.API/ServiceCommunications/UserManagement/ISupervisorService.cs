﻿using DispatchProduct.HttpClient;
using Ghanim.DataManagement.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.DataManagement.API.ServiceCommunications.UserManagement
{
    public interface ISupervisorService : IDefaultHttpClientCrud<UserManagementServiceSettings, SupervisorViewModel, SupervisorViewModel>
    {
        Task<ProcessResultViewModel<List<SupervisorViewModel>>> GetSupervisorsByDivisionId(int divisonId);
        Task<ProcessResultViewModel<SupervisorViewModel>> GetSupervisorByDivisionId(int divisonId);
    }
}