﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
   public interface ICostCenter : IBaseEntity
    {
        string Name { get; set; }
    }
}
