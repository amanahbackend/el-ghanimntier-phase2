﻿using DispatchProduct.BuildingBlocks.BaseEntities.IEntities;
using Ghanim.DataManagement.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.IEntities
{
    public interface IGovernorates : IBaseEntity
    {
        string Name { get; set; }
        int Gov_No { get; set; }
        List<Areas> Areas { get; set; }
    }
}
