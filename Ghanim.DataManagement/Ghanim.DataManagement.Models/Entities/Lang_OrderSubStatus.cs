﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
    public class Lang_OrderSubStatus : BaseEntity,ILang_OrderSubStatus
    {
        public int FK_OrderSubStatus_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
    }
}
