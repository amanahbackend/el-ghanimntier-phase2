﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
    public class OrderProblem : BaseEntity, IOrderProblem
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int ExceedHours { get; set; }
    }
}
