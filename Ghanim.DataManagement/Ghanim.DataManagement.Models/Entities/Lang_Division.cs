﻿using DispatchProduct.BuildingBlocks.BaseEntities.Entities;
using Ghanim.DataManagement.Models.IEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ghanim.DataManagement.Models.Entities
{
    public class Lang_Division : BaseEntity, ILang_Division
    {
        public int FK_Division_ID { get; set; }
        public int FK_SupportedLanguages_ID { get; set; }
        public string Name { get; set; }
        [NotMapped]
        public Division Division { get; set; }
        [NotMapped]
        public SupportedLanguages SupportedLanguages { get; set; }
    }
}
