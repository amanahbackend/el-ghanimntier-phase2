﻿using DispatchProduct.HttpClient;
using Ghanim.Dropout.API.ServiceSettings;
using Ghanim.Dropout.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Ghanim.Dropout.API.ServiceCommunications.Identity
{
   public interface IIdentityService : IDefaultHttpClientCrud<IdentityServiceSettings, string, List<UserDeviceViewModel>>
    {
        Task<ProcessResultViewModel<List<UserDeviceViewModel>>> GetUserDevices(string userId);
    }
}
