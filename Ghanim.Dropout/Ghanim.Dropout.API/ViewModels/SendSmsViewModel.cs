﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ghanim.Dropout.API.ViewModels
{
    public class SendSmsViewModel
    {
        public string Message { get; set; }
        public string PhoneNumber { get; set; }
    }
}
